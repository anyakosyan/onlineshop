<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductModel extends Model
{
    //
    public $table = 'product';
    public $timestamps = false;

    function image(){
    	//liqy nkarnery
    	return $this->hasMany('App\ProductImageModel','product_id');
    }
    function user(){
    	return $this->belongsTo('App\UserModel','user_id');
    }

}
