<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HeartModel extends Model
{
    public $table ='wishlist';
    public $timestamps = false;
}
