<?php

namespace App\Http\Controllers;
   
use Illuminate\Http\Request;
use App\ProductModel;
use App\BasketModel;
use Session;
use Stripe;
   
class StripePaymentController extends Controller
{
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripe()
    {
        return view('stripe');
    }
  
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripePost(Request $request)
    {
    	$id = Session::get('userId');
        $confirmProduct = BasketModel::where(['user_id' =>$id]) ->get();
        $allPrice=0;
        foreach ($confirmProduct as $key) {
        	// dd($key);
        	$price = $key->product['price'] * $key['count'] ;
        	$allPrice += $price;
        	$count = ProductModel::where(['id' =>$key['product_id']]) ->first();
        	$newCoutn= $count['count'];
        	$a = $newCoutn - $key['count'];
        	ProductModel::where(['id' =>$key['product_id']]) ->update(['count' =>$a]);
        	BasketModel::where(['user_id' =>$id]) ->delete();
        	$productEmpty = ProductModel::where(['id' =>$key['product_id']]) ->first();
        	if($productEmpty['count'] == 0) {
                ProductModel::where(['id' =>$key['product_id']]) ->delete();
            }
        }
        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        Stripe\Charge::create ([
                "amount" => $allPrice*100,
                "currency" => "usd",
                "source" => $request->stripeToken,
                "description" => "Test payment from itsolutionstuff.com." 
        ]);
        Session::flash('success', 'Payment successful!');
          
        return back();
    }
}
