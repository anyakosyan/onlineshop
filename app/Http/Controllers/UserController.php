<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\UserModel;
use App\ProductModel;
use App\ProductImageModel;
use App\BasketModel;
use App\HeartModel;
use Illuminate\Support\Facades\DB;

class UserController extends Controller {
	function index() {
		// return view('shop');
		// dd('a');
			$id = Session::get('userId');
			$productBuyCount = BasketModel::where('user_id', $id) ->count();
			// dd($productBuyCount);
//            dd($productBuyCount)
            $user = UserModel::where('id',$id)->first();
			return view('shop',compact('user','productBuyCount'));
	}
	function account() {
			$id = Session::get('userId');
			$user = UserModel::where('id',$id)->first();
			return view('my_account',compact('user'));
	}
	function editAcoount(Request $x) {
		$validaetData = $x ->validate([
			'name' => 'required',
			'surname' => 'required',
			'age' => 'required|numeric'

		]);
		 UserModel::where('id',Session::get('userId'))->update(['name'=>$x->name,'surname' =>$x->surname,'age' => $x->age]);
		return redirect('/my-account');
	}
	function logout() {
		// $id = Session::get('userId');
		Session::forget('userId');
		return redirect('/login');
	}
	function product() {
        $id = Session::get('userId');
        $productBuyCount = BasketModel::where('user_id', $id) ->count();
        return view('add_product',compact('productBuyCount'));
	}
	function addProduct(Request $x) {
		$validateData = $x ->validate([
			// 'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'images' => 'required',
			'name' => 'required',
			'price' => 'required|numeric',
			'count' => 'required|numeric',
			'description' => 'required'
		]);
		$product = new ProductModel;
		$product ->name = $x ->name;
		$product ->price =$x ->price;
		$product ->count =$x ->count;
		$product ->description = $x ->description;
		$product ->user_id = Session::get('userId');
		$product->save();
        $id = $product->id;
		 if($files=$x->file('images')){
        foreach($files as $file){
            $name=time().$file->getClientOriginalName();
            $file->move('image',$name);
		 	$imagee = new ProductImageModel;
            $imagee ->image = $name;
            $imagee ->product_id = $id;
            $imagee->save();
        }
    }
//    return view('add_product', compact());
        return redirect() ->back()->with('productMessage', 'Your product successfully added');
	}
	function allProduct() {
       $all_product = ProductModel::where('active', '1') ->get();
        $id = Session::get('userId');
        $productBuyCount = BasketModel::where('user_id', $id) ->count();
//       dd($all_product[0] ->id);
        	$id = Session::get('userId');
            return view('all_product', compact(["all_product","id","productBuyCount"]));
    }
    function myProduct() {
	    $id = Session::get('userId');
	    $my_product = ProductModel::where('user_id',$id) ->get();
        $productBuyCount = BasketModel::where('user_id', $id) ->count();
	    // dd($my_product[0]->image);
	    // dd($my_product[0]->user);
            return view('my_product',compact('my_product', 'productBuyCount'));
	}


	function productItem($id){
		// print_r($id);
		$product_item = ProductModel::where('id',$id) ->first();
        $productBuyCount = BasketModel::where('user_id', Session::get('userId')) ->count();
        $hasThisProduct =  BasketModel::where(['user_id' => Session::get('userId'), 'product_id' =>$id]) ->first();
        $user_id = Session::get('userId');
        $heartProduct = HeartModel::where(['user_id' =>$user_id, 'product_id' =>$id]) ->first();
        $id = Session::get('userId');
        $text = '';
        if(!empty($hasThisProduct)) {
            $text = 'Added';
            $value = $hasThisProduct ->count;
        }else {
            $text ="Add to cart";
            $value = 0;
        }
        return view('product_item',compact(['product_item','id','productBuyCount','text','value','heartProduct']));

	}
	function productEdit($id, Request $x){
	    $validateData = $x ->validate([
	        'name' => 'required',
            'description' =>'required',
            'price' => 'required|numeric',
            'count' => 'required|numeric'
        ]);
	    ProductModel::where('id',$id)->update(['name' => $x ->name,'description' => $x ->description, 'price' => $x ->price,'count' => $x ->count, 'active' => '0']);
	     if($files=$x->file('images')){
        foreach($files as $file){
            $name=time().$file->getClientOriginalName();
            $file->move('image',$name);
		 	$imagee = new ProductImageModel;
            $imagee ->image = $name;
            $imagee ->product_id = $id;
            $imagee->save();
        }
	}
        return redirect('/product-item'.'/'.$id);
    }
    function productDelete($id) {
	    ProductModel::where('id',$id) ->delete();
        return redirect('/my-product');
    }
    function deleteProductImage(Request $x) {
    	ProductImageModel::where('id',$x->data)->delete();
    	return $x ->data;
    }
    function productBuy(Request $x) {
	    $product = ProductModel::where('id',$x ->id) ->first();
	    $productCount = $product ->count;
        $validateData = $x ->validate([
           'count'=> "required|min:1|max:$productCount|numeric"
       ]);
      $basket = new BasketModel;
      $id = Session::get('userId');
      $hasThisProduct =  BasketModel::where(['user_id' => $id, 'product_id' =>$x ->id]) ->first();
      $editProductCount = '';
       if(empty($hasThisProduct)) {
           if($productCount>=$x ->count) {
               $basket ->product_id = $x ->id;
               //$basket ->user_id = $x ->user['id'];
               $basket ->user_id = $id;
               $basket ->count = $x ->count;
//             $basket ->price = $x ->count * $product ->price;
               $basket ->save();
           }
           $editProductCount = 'add';
       }else if($productCount>=$x->count){
           BasketModel::where(['user_id' => $id, 'product_id' =>$x ->id])->update(['count'=>$x ->count]);
           $editProductCount = 'editCount';
       }
        return $editProductCount;
    }
      function checkOut() {
          $id = Session::get('userId');
    //      $hasThisProduct =  BasketModel::where(['user_id' => $id]) ->first();
          $productBuyCount = BasketModel::where('user_id', $id) ->count();
          $hasThisProduct =  BasketModel::where(['user_id' => $id]) ->get();
          return view('check_out',compact('hasThisProduct','productBuyCount'));
      }
      function deleteCartProduct(Request $x) {
          $id = Session::get('userId');
          BasketModel::where(['user_id' => $id, 'product_id' => $x ->id]) ->delete();
      }
//    function confirmCartProduct(Request $x) {
//        $id = Session::get('userId');
//        $product = ProductModel::where(['user_id' => $id, 'product_id' => $x ->id]) ->first();
//        $confirmProduct = BasketModel::where(['user_id' => $id, 'product_id' => $x ->id]) ->first();
//        BasketModel::where(['user_id' => $id, 'product_id' => $x ->id]) ->update(['price' => $confirmProduct['count'] * $product['price']]);
//    }
    function confirmCartProduct(Request $x) {
        $id = Session::get('userId');
       BasketModel::where(['id' =>$x ->id]) ->update(['active' =>1]);
    }
    function heartingProduct(Request $x) {
        $id = Session::get('userId');
        $heartProduct = HeartModel::where(['user_id' =>$id, 'product_id' =>$x ->id]) ->first();
        $issetHeart = '';
        if(!empty($heartProduct)) {
            HeartModel::where(['user_id' =>$id, 'product_id' =>$x ->id]) ->delete();
            $issetHeart = 'add';
        }else {
            $product = new HeartModel;
            $product ->product_id = $x ->id;
            $product ->user_id = $id;
            $product ->save();
            $issetHeart = 'added';
//            return redirect() ->back()->with('heart', 'isset heart');
        }
        return $issetHeart;
    }
    function myShop() {
	    $id = Session::get('userId');
	    $myShop = BasketModel::where('user_id',$id) ->get();
        $productBuyCount = BasketModel::where('user_id', $id) ->count();
	    return view('my_shop',compact('myShop','productBuyCount'));
    }
    }
//$buyCount = BasketModel::orderBy('user_id',$id) ->get();
//

