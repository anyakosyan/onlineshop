<?php
namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\UserModel;
use App\ProductModel;
use App\ProductImageModel;
class AdminController extends Controller {
	function index() {
		$users = UserModel::where('status', 'user') ->get();
		$products = ProductModel::where('active','0')->get();
		return view('admin',compact(['users','products']));
	}
//	function activateProduct() {
//		$product = ProductModel::where('active','0')->get();
//		dd($product);
//		return redirect('/online-shop-admin');
//	}
	function acceptProduct(Request $x) {
        ProductModel::where('id',$x->data)->update(['active' => '1']);
    }
    function deleteProductMessage(Request $x) {
        $validateData = $x ->validate([
            'message' => "required"
        ]);
        ProductModel::where('id',$x->data)->update(['active' => $x ->message]);
    }
	function userBlocking(Request $x) {
	    UserModel::where('id',$x ->id)->update(['block' => $x ->time]);
    }
}
