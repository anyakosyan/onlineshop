<?php

namespace App\Http\Controllers;
use Mail;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\UserModel;
// class RegController extends Controller
// {
//    function index() {
//     return view('signup', ['name' => 'Aram']);
//    }
// }
//stex vorpeserkrord parametr hetners tvyal neq tanum
//hto es name keya darnuma $name popoxakan php kodum
//controlleric view tvyal texapoxela hamar lav tarberak e compact@
class RegController extends Controller
{
   function index() {
    $name = 'Aram';
    return view('signup', compact('name'));
   }
   function signup(Request $x) {
   	// dd($x->name);
   	 $validatedData = $x->validate([
        'name' => 'required|max:25',
        'surname' => 'required|max:25',
        'email' => 'required|email',
        'age' => 'required|numeric',
        'password' => 'required|max:12|min:6',
		'confpassword' => 'required|max:12|min:6|same:password'
    ]);
     $user = new UserModel;
     $user ->name = $x ->name;
     $user ->surname = $x ->surname;
     $user ->age = $x ->age;
     $user ->email = $x ->email;
     $user ->password = Hash::make($x->password);
     $user ->save();
     $email = $x ->email;
     $name = $x ->name;
     $hash = md5($email.$user ->id);
      Mail::send('mail', ['name' =>$name,'email' =>$email, 'hash' =>$hash], function($message) use ($email,$name) {
         $message->to($email, $name)->subject
            ('Eji hastatum');
      });
      // sendi jamanak aragin parametr@ blade.php file,ete uzum enq inch vor tiv kam text menak tanenq dra hamar sendi poxaren grum enq row
      // erkrord paramet@ view inch vor uzum enq tanqn et tvyalna
      //qani vor $emal@ u $name global chen kcum enq usov $ use ($email,$name)
      // md5()@ kodavoruma bayc misht nuyna kodna berum
     return redirect('/login');
  }
  function userCheck($email,$hash) {
    $data = UserModel::where('email',$email) ->first();
    if($data) {
      $id = $data ->id;
      $hash1 = md5($email.$id);
      if($hash1 == $hash) {
        UserModel::where('email',$email) ->update(['active' => 1]);
               return redirect('/login');
      }
    }
    
  }
  function login() {
        return view('login');
    }
     function personLogin(Request $x) {
        $validatedData = $x ->validate([
            'email' => 'required',
            'password' =>'required'
        ]);
       $arr = UserModel::where('email',$x->email) ->first();
       if(empty($arr)) {
       return redirect()->back()->withErrors(['emailerror' => 'nman emailov mard chka']);
       }else {
        if(Hash::check($x ->password,$arr->password)) {
          if($arr ->status == "user") {
            // dd('lyaaa');
            if($arr ->active == 1) {
                if($arr ->block - time() > 0) {
                    return redirect() ->back()->withErrors(['blockUser' =>'duq der blocki mej eq']);
                }else {
                    Session::put('userId', $arr ->id);
                    return redirect('/shop');
                }
            }else {
            return redirect() ->back()->withErrors(['activateUser' =>'nayel emaili message']);
          }
          }else {
              if($arr ->active == 1) {
                  Session::put('adminId', $arr ->id);
                  return redirect('/online-shop-admin');
              }else {
                  return redirect() ->back()->withErrors(['activateUser' =>'nayel emaili message']);
              }
          }
        }else{
            $email = $x->email;
            $user_id = $arr['id'];
            $hash =md5($email.$user_id);
          return redirect() ->back()->withErrors(['passworderror' =>'passwordner@ chen ham@nknum','forgetPassword' => 'Forget password?','address' => $email.'/'.$hash]);
        }
       }
       // wheri tex@ vor grenq all kberi et tabli sax tvyalner@,firsti tex@@ vor grenq get kver bolor emailner@ voronq havasar en $emailin
       // tan@ anel sessian ete sax normala sessiayum pahel idin mardu
    }
    function forgotPassword($email,$hash) {
       $user = UserModel::where('email',$email) ->first();
       $name = $user['name'];
       $hash1 = md5($email.$user['id']);
       $pin = mt_rand(10000, 99999);
       Session::put('code', $pin);
       Session::put('email', $email);
       if($hash == $hash1) {
           Mail::send('mail_code', ['email' => $email, 'hash' =>$hash, 'pin' => $pin], function ($message) use($email,$name) {
               $message ->to($email,$name) ->subject('Your code');
           });
       }
        return view('forget_password');
    }
    function newPassword(Request $x){
       $code = Session::get('code');
       $email = Session::get('email');
        $validateData = $x ->validate([
            'code' => "required",
            'password' => 'required|max:12|min:6',
            'confPassword' => 'required|same:password',
        ]);
//        dd($code);
        if($x ->code == $code) {
            UserModel::where('email',$email) ->update(['password' => Hash::make($x ->password)]);
            return redirect('/login');
        }else {
            return redirect()->back()->withErrors(['errorCode' => 'wrong code']);
        }
    }
   }

// tan@ nayum en blade for if ev ayln,routn enq nayum,controlleric view tvyal texapoxelu tarberakner@
