<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BasketModel extends Model
{
    public $table ='basket';
    public $timestamps = false;
    function user(){
        return $this ->belongsTo('App\UserModel', 'user_id');
    }
    function product() {
        return $this ->belongsTo('App\ProductModel', 'product_id');
    }

}

