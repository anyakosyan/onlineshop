<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImageModel extends Model
{
    //
    public $table = 'product_img';
    public $timestamps = false;
 
}
