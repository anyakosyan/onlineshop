/*
Navicat MySQL Data Transfer

Source Server         : connection
Source Server Version : 100138
Source Host           : localhost:3306
Source Database       : onlineshop

Target Server Type    : MYSQL
Target Server Version : 100138
File Encoding         : 65001

Date: 2019-12-05 17:50:50
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for basket
-- ----------------------------
DROP TABLE IF EXISTS `basket`;
CREATE TABLE `basket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of basket
-- ----------------------------
INSERT INTO `basket` VALUES ('40', '20', '21', '1');
INSERT INTO `basket` VALUES ('44', '21', '21', '2');
INSERT INTO `basket` VALUES ('45', '22', '21', '2');

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT '',
  `user_id` int(11) DEFAULT NULL,
  `active` varchar(255) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE,
  CONSTRAINT `product_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('18', 'ced', '300', '4', 'lav ced', '9', 'qani vor tank e');
INSERT INTO `product` VALUES ('20', 'shor1', '100', '2', 'erertsertert', '19', '1');
INSERT INTO `product` VALUES ('21', 'shor2', '21000', '4', 'dhxdhxhg', '19', '1');
INSERT INTO `product` VALUES ('22', 'admin', '6500', '8', 'dfgdfgdgfdf', '19', '1');
INSERT INTO `product` VALUES ('23', 'shor3', '200', '6', 'lav shor3', '21', 'hjk');
INSERT INTO `product` VALUES ('24', 'wine', '120', '12', 'abcd', '21', 'ii');
INSERT INTO `product` VALUES ('25', 'wine', '120', '12', 'abcd', '21', '1');
INSERT INTO `product` VALUES ('26', 'wine2', '150', '13', 'abcde', '21', 'gsgsg');
INSERT INTO `product` VALUES ('27', 'wine3', '130', '5', 'asdfghj', '21', '1');
INSERT INTO `product` VALUES ('29', 'jk', '12', '1', 'fgj', '21', '1');

-- ----------------------------
-- Table structure for product_img
-- ----------------------------
DROP TABLE IF EXISTS `product_img`;
CREATE TABLE `product_img` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `product_id` (`product_id`) USING BTREE,
  CONSTRAINT `product_img_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of product_img
-- ----------------------------
INSERT INTO `product_img` VALUES ('5', '18', '1574435464Nike Air Max 200 (3).jpg');
INSERT INTO `product_img` VALUES ('6', '18', '1574435464Nike SB Zoom Janoski RM 3.jpg');
INSERT INTO `product_img` VALUES ('11', '20', '1575212005registration-form-1.jpg');
INSERT INTO `product_img` VALUES ('12', '21', '1575212025bg-registration-form-1.jpg');
INSERT INTO `product_img` VALUES ('13', '22', '1575212045bg-registration-form-1.jpg');
INSERT INTO `product_img` VALUES ('14', '22', '1575212045registration-form-1.jpg');
INSERT INTO `product_img` VALUES ('15', '24', '15755270835cd93b0d0171c_1557740301_20190513.png');
INSERT INTO `product_img` VALUES ('16', '25', '15755271465cd93b0d0171c_1557740301_20190513.png');
INSERT INTO `product_img` VALUES ('17', '26', '15755272835cd93b0d0171c_1557740301_20190513.png');
INSERT INTO `product_img` VALUES ('18', '27', '15755280505cd93e4a9b7da_1557741130_20190513.png');
INSERT INTO `product_img` VALUES ('20', '29', '15755283355cd93b0d0171c_1557740301_20190513.png');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT 'user',
  `active` int(11) DEFAULT '0',
  `block` varchar(255) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('5', 'Hayk', 'Mkrtchyan', '24', 'hayk@mail.ru', '$2y$10$8zef4wGw9OjB/ZKS6sXEWOfqrpQ1fmfI7jrEwovr/EBJJldWKS5Eq', 'user', '0', '0');
INSERT INTO `user` VALUES ('7', 'anya', 'kosyan', '24', 'anya.kosyan@gmail.com', '$2y$10$HeyFIYXrPrvQ.eLGFD.yTuTlQnngMGkPTsXbAu9DqH5Wip2h7QTSm', 'user', '0', '0');
INSERT INTO `user` VALUES ('9', 'levonafaf', 'kosyan', '17', 'levon@mail.ru', '$2y$10$IaR6jSxz6B0wFnIhhRAnKuuwROVQOJBofVf1J.mPDhGyTRwnylOZy', 'user', '0', '0');
INSERT INTO `user` VALUES ('19', 'anya', 'kosyan', '24', 'kosyan.anya1@gmail.com', '$2y$10$Jsi5ymoOgE6V3CzXBNaiAujSvjo89aoFgJivBnzhPbfyAvVtd4Sku', 'admin', '1', '0');
INSERT INTO `user` VALUES ('21', 'anya', 'kosyan', '24', 'kosyan.anya@mail.ru', '$2y$10$cq0g4N3C7GGsdsY5FTZPkuTj3R8gf86aQJUveB/xa9OZhAISa6vHC', 'user', '1', '0');

-- ----------------------------
-- Table structure for wishlist
-- ----------------------------
DROP TABLE IF EXISTS `wishlist`;
CREATE TABLE `wishlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wishlist
-- ----------------------------
INSERT INTO `wishlist` VALUES ('33', '20', '21');
SET FOREIGN_KEY_CHECKS=1;
