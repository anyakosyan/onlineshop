
////upload image
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('.image_block').append(`<div class="position-relative image_block-item"><img id="blah" src="${e.target.result}" alt="your image" /><button class="btn delete-product-img" type="button"><i class="fa fa-times"></i></button></div>`)
        };
        reader.readAsDataURL(input.files[0]);
    }
}
////delete product image
    $(document).on('click', '.delete-product-img', function () {
        var data = $(this).attr('data-id');
         $.ajax({
              url: '/deleteProductImage',
              type: 'post',
              data: {data, _token:$('.token').val()},
              success: (r)=> {
                $(this).parent('.image_block-item').remove();
                console.log(r);
              }
            })
    })
////admin accept product
    $('.admin-mine #accept').on('click',function () {
        var data = $(this).attr('data-id');
        $.ajax({
            url: '/accept-product',
            type: 'post',
            data: {data, _token:$('.token').val()},
            success: (r)=> {
                $(this).parents('.product_block-item').remove();
                console.log(r);
            }
        })
    })
////add data-id in modal save button
    $('.admin-mine #delete').on('click',function () {
        var id= $(this).attr('data-id');
        $(this).parents('body').find('#delete-product-modal #message_save').attr('data-id', id);
    })
// save delete product message
// message_save
$('#message_save').on('click',function () {
    var data= $(this).attr('data-id');
    var message = $(this).parents('.modal').find('textarea').val();
    $.ajax({
        url: '/delete-product-message',
        type: 'post',
        data: {data, message,_token:$('.token').val()},
        success: (r)=> {
            $(this).parents('body').find(("#delete[data-id = ']" +data+"'")).parents('.product_block-item').remove();
            console.log(message);
        }
    })
})
////add data-id in modal block save buton
$('.admin-mine #user_block').on('click',function () {
    var id= $(this).attr('data-id');
    $(this).parents('body').find('#block_user_save').attr('data-id', id);
})
///admin block
$('#block_user_save').on('click',function () {
    var id= $(this).attr('data-id');
    var time = $(this).parents('.modal').find('input').val()*60 + Date.now();
    // console.log(time)
    $.ajax({
        url: '/user-blocking',
        type: 'post',
        data: {id, time,_token:$('.token').val()},
        success: (r)=> {
            console.log(time);
        }
    })
})

////product add to cart
var buyCount = $('.badge').text();
$('#add_cart').on('click',function () {
    var id = $(this).attr('data-id');
   $(this).parents('.single-product-details').find('.error').remove();
    var count = $(this).parents('.single-product-details').find('#product_quantity').val();
    // if(isNaN(count) || count <= 0){
    //     var div = $(`<div class="error">Please choose product count</div>`);
    //     $(this).parents('.single-product-details').find('.quantity-box').append(div);
    // }else {
    //     var parseBuyCount = parseInt(buyCount);
    //     parseBuyCount +=parseInt(count);
    //     console.log(parseBuyCount);
        // if(buyCount != 0) {
        //     $(this).parents('body').find('.badge').text(parseBuyCount);
        // }
        $.ajax({
            url: '/add-to-cart',
            type: 'post',
            data: {id,count,_token:$('.token').val()},
            success: (r)=> {
                var text = $(`<div class="message-text"></div>`);
                $(this).parents('main').find('.success-message').append(text);
                if(r == 'add') {
                    var parseBuyCount = parseInt(buyCount);
                    parseBuyCount +=parseInt(count);
                    console.log(parseBuyCount);
                    $(this).parents('body').find('.badge').text(parseBuyCount);
                    $(this).text('Added');
                    text.text('Product successfully added');
                }else if(r == 'editCount') {
                    console.log('editCount')
                    text.text('Product qty successfully updated');
                }
                setTimeout(text.fadeOut(4000),7000)
            },
            error:  (data)=> {
                // JSON.parse = $.parseJSON nuyn banern en
             if(data) {
                 var errors = $.parseJSON(data.responseText);
                 console.log(typeof errors);
                 var div = $(`<div class="error">${errors.errors['count'][0]}</div>`);
                 $(this).parents('.single-product-details').find('.quantity-box').append(div);

             }else {
                 console.log('lalalal');
             }
            }
        })

    //

})

$('.product_delete').on('click', function () {
    var id = $(this).attr('data-id');
    $.ajax({
        url: '/delete-cart-product',
        type: 'post',
        data: {id,_token:$('.token').val()},
        success: (r)=> {
            $(this).parents('tr').remove();
        }
    })
})
///product successfully added message
$('.add-product_main .product-added').fadeOut(6000);

// $('.product_confirm').on('click', function () {
//     var id = $(this).attr('data-id');
//     console.log(id)
//     $.ajax({
//         url: '/confirm-cart-product',
//         type: 'post',
//         data: {id,_token:$('.token').val()},
//         success: (r)=> {
//             console.log('khkh')
//             // $(this).parents('tr').remove();
//         }
//     })
// })
///hearting product
$('#heart').on('click',function () {
    var id = $(this).attr('data-id');
    $.ajax({
        url: '/heart-product',
        type: 'post',
        data: {id,_token:$('.token').val()},
        success: (r)=> {
            if(r == 'add') {
                $(this).html('<i class="fas fa-heart"></i> Add to wishlist');
            }else if (r == 'added') {
                $(this).html('<i class="fas fa-heart" style="color: darkred"></i> Added to wishlist');
            }

        }
    })
})
