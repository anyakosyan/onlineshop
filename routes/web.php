<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
// Route::get('/signup', function () {
//     return view('signUp');
// });sra poxaren karanq grenq
Route::get('/signup','RegController@index');
Route::post('/signup','RegController@signup');
Route::get('/hastatum/{email}/{hash}','RegController@userCheck');
Route::get('/forgot-password/{email}/{hash}','RegController@forgotPassword');
Route::post('/forgot-password','RegController@newPassword');
Route::get('/login', 'RegController@login');
Route::post('/login','RegController@personLogin');
Route::get('/shop', 'UserController@index')->middleware('checkuser');
Route::get('/admin-shop', 'AdminController@index');
Route::get('/my-account', 'UserController@account')->middleware('checkuser');
Route::post('/my-account', 'UserController@editAcoount');
Route::get('/logout', 'UserController@logout');
Route::get('/add-product','UserController@product') ->middleware('checkuser');
Route::post('/add-product','UserController@addProduct');
Route::get('/all-product','UserController@allProduct') ->middleware('checkuser');
Route::get('/my-product','UserController@myProduct') ->middleware('checkuser');
Route::get('/product-item/{a}','UserController@productItem') ->middleware('checkuser');
Route::post('/product-item/{a}','UserController@productEdit');
Route::post('/product-item-delete/{a}','UserController@productDelete');
Route::post('/deleteProductImage','UserController@deleteProductImage');
Route::get('/online-shop-admin','AdminController@index');
Route::post('/accept-product','AdminController@acceptProduct');
Route::post('/delete-product-message','AdminController@deleteProductMessage');
Route::post('/user-blocking','AdminController@userBlocking');
Route::post('/add-to-cart','UserController@productBuy');
Route::get('/check-out','UserController@checkOut') ->middleware('checkuser');
Route::post('/delete-cart-product','UserController@deleteCartProduct');
Route::post('//heart-product','UserController@heartingProduct');
Route::get('/my-shop','UserController@myShop');
//Route::post('/confirm-cart-product','UserController@confirmCartProduct');
//Route::post('/cart-count','UserController@productBuyCount');


Route::get('stripe', 'StripePaymentController@stripe');
Route::post('stripe', 'StripePaymentController@stripePost')->name('stripe.post');



