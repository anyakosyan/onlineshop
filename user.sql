/*
Navicat MySQL Data Transfer

Source Server         : armenhomework
Source Server Version : 100406
Source Host           : localhost:3306
Source Database       : onlineshop

Target Server Type    : MYSQL
Target Server Version : 100406
File Encoding         : 65001

Date: 2019-11-19 20:56:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT 'user',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('5', 'Hayk', 'Mkrtchyan', '24', 'hayk@mail.ru', '$2y$10$8zef4wGw9OjB/ZKS6sXEWOfqrpQ1fmfI7jrEwovr/EBJJldWKS5Eq', 'user');
INSERT INTO `user` VALUES ('6', 'Ani', 'Sargsyan', '25', 'ani@mail.ru', '$2y$10$z7gd0b3Fm2xe/V0cX7W4iOXFxyrC4.h2bAU4/7hEC7dFBX3BeqmVW', 'user');
INSERT INTO `user` VALUES ('7', 'anya', 'kosyan', '24', 'anya.kosyan@gmail.com', '$2y$10$HeyFIYXrPrvQ.eLGFD.yTuTlQnngMGkPTsXbAu9DqH5Wip2h7QTSm', 'user');
INSERT INTO `user` VALUES ('9', 'levonafaf', 'kosyan', '17', 'levon@mail.ru', '$2y$10$IaR6jSxz6B0wFnIhhRAnKuuwROVQOJBofVf1J.mPDhGyTRwnylOZy', 'user');
SET FOREIGN_KEY_CHECKS=1;
