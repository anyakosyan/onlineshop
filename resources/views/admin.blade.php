<!DOCTYPE html>
<html lang="en">
<head>
	 <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Site Metas -->
    <title>@yield('title') Admin</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Site Icons -->
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{URL::asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/slick.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/admin.css')}}">

</head>
<body>
	<main class="admin-mine">
		<input type="hidden" class="token" value="{{csrf_token()}}">
		<div class="container">
			<h1 class="admin-title text-center">Admin Page</h1>
			<div class="text-center user-title"><h2>User</h2></div>
			{{-- <div>Duq uneq <a href=""></a> harcum</div> --}}
			<div class="user_block row">
				<?php $i = 0 ?>
				@foreach($users as $user)
						<?php $i++ ?>
					<div class="col-sm-12 col-md-3 align-items-stretch">
						<div class="user_block-item text-center">
							<h3 class="text-center name"><strong>{{$i}}.</strong> {{$user['name']}} {{$user['surname']}}</h3>
							<h4 class="text-center age">{{$user['age']}}</h4>
                            @if($user['block'] - time() <= 0)
                                <button class="btn base-btn" type="button" id="user_block" data-id="{{$user['id']}}" data-toggle="modal" data-target="#block_user_modal">Blocking</button>
                                @endif
						</div>
					</div>
				@endforeach
			</div>
			<div class="user_block">
				<div class="text-center user-title"><h2>Products</h2></div>
				@foreach($products as $product)
					<div class="product_block-item">
						<div class="row">
							<div class="col-sm-12 col-md-6">
								<div class="product-image">
									@foreach($product ->image as $productImage)
										<div>
											<img src="{{URL::to('/image'). '/' .$productImage['image']}}" alt="">
										</div>
									@endforeach
								</div>
							</div>
							<div class="col-sm-12 col-md-6">
								<div class="product-info">
									<div><strong>name: </strong><h5 class="d-inline-block">{{$product['name']}}</h5></div>
									<div><strong>price: </strong><h5 class="d-inline-block">{{$product['price']}}$</h5></div>
									<div><strong>count: </strong><h5 class="d-inline-block">{{$product['count']}}</h5></div>
									<p>{{$product['description']}}</p>
									<button type="button" class="btn" id="accept" data-id="{{$product['id']}}">Accept product</button>
									<button type="button" class="btn" id="delete" data-id="{{$product['id']}}" data-toggle='modal' data-target="#delete-product-modal">Delete product</button>
								</div>
							</div>
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</main>
	<div class="modal" id="delete-product-modal">
		<div class="modal-dialog">
			<div class="modal-content">

				<!-- Modal Header -->
				<div class="modal-header">
					<h4 class="modal-title">Add message for delete product</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>

				<!-- Modal body -->
				<div class="modal-body">
					<textarea class="w-100" id="message"  rows="4" ></textarea>
					@if($errors->has('message'))
						<span class="error">{{ $errors->first('message') }}</span>
					@endif
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
						<button class="btn btn-danger" id="message_save" >Save</button>
					</div>
				</div>
			</div>
		</div>
	</div>
    <div class="modal" id="block_user_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">User Blocking</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <input type="number">
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button class="btn btn-danger" id="block_user_save">Save</button>
                </div>

            </div>
        </div>
    </div>
	<script src="{{URL::asset('js/jquery-3.2.1.min.js')}}"></script>
	<script src="{{URL::asset('js/popper.min.js')}}"></script>
	<script src="{{URL::asset('js/bootstrap.min.js')}}"></script>
	<!-- ALL PLUGINS -->
	<script src="{{URL::asset('js/jquery.superslides.min.js')}}"></script>
	<script src="{{URL::asset('js/bootstrap-select.js')}}"></script>
	<script src="{{URL::asset('js/slick.js')}}"></script>
	<script src="{{URL::asset('js/main.js')}}"></script>
	<script>
		// product-image
		  $('.product-image').slick({
		      });
	</script>
</body>
</html>
