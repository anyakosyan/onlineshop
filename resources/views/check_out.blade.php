@extends('layouts/user_profile_layout')
@section('title','onlineshop')
@section('container')
    <main class="check-out_main">
        <input type="hidden" class="token" value="{{csrf_token()}}">
        <div class="container">
            @if($hasThisProduct)
                <table class="check-out-table w-100 text-center">
                    @foreach($hasThisProduct as $product)
                        <tr>
                            <td><button data-id="{{$product['product_id']}}" class="btn product_delete"><i class="fas fa-trash-alt"></i></button></td>
                            <td><a data-id="{{$product['id']}}" class="btn" href="{{URL::to('/product-item').'/'.$product['product_id']}}"><i class="fas fa-edit"></i></a></td>
                            {{--<td>--}}
                            {{--<img src="{{URL::to('/image').'/'.$product ->product ->image['image'][0]}}" alt="">--}}
                            {{--</td>--}}
                            <td>
                                <h3>{{$product ->product['name']}}</h3>
                            </td>
                            <td>
                                <h3>{{$product['count']}}</h3>
                            </td>
                        </tr>
                    @endforeach
                </table>
            @endif
            <div class="text-center mt-4">
                <a class="btn all-confirm" href="{{URL::to('/stripe')}}">Confirm</a>
            </div>
        </div>
    </main>
@endsection
@section('js')
@endsection
