@extends('layouts/user_profile_layout')
@section('title','onlineshop')
@section('container')
<!-- Start Top Search -->
    <main class="add-product_main">
        @if(session()->has('productMessage'))
            <div class="product-added">
                {{ session()->get('productMessage') }}
            </div>
        @endif
        <div class="container">
            <form action="/add-product" method="post" enctype='multipart/form-data' id="add-product_form">
                {{csrf_field()}}
                <div class="form-wrapper">
                    <input type="file" name="images[]" multiple="">
                    @if($errors ->has('images'))
                        <span class="error d-block">{{ $errors ->first('images') }}</span>
                    @endif
                </div>
                <div class="form-wrapper">
                    <label for="name">Product name</label>
                    <input type="text" placeholder="" class="form-control" id='name' name="name">
                    @if($errors ->has('name'))
                        <span class="error">{{ $errors ->first('name') }}</span>
                    @endif
                </div>
                <div class="form-wrapper">
                    <label for="price">Product price</label>
                    <input type="number" placeholder="" class="form-control" id='price' name="price">
                    @if($errors ->has('price'))
                        <span class="error">{{ $errors ->first('price') }}</span>
                    @endif
                </div>
                <div class="form-wrapper">
                    <label for="count">Product count</label>
                    <input type="number" placeholder="" class="form-control" id='count' name="count">
                    @if($errors ->has('count'))
                        <span class="error">{{ $errors ->first('count') }}</span>
                    @endif
                </div>
                <div class="form-wrapper" style="resize: none;">
                    <label for="description">Product description</label>
                    <textarea class="form-control" id='description' name="description"></textarea>
                    @if($errors ->has('description'))
                        <span class="error">{{ $errors ->first('description') }}</span>
                    @endif
                </div>
                <div class="form-wrapper">
                    <button class="btn add-product" id="add-product" name="add-product">Add Product</button>
                </div>
            </form>
        </div>
        <!-- End Instagram Feed  -->
    </main>
@endsection
