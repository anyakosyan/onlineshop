@extends('layouts/user_profile_layout')
@section('css')
@endsection
@section('container')
    <main class="my-shop_main">
        <div class="all-title-box">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>Shop</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Shop</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row product-categorie-box">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade show active" id="grid-view">
                        <div class="row">
                            @foreach($myShop as $product)
                                <div class="col-sm-6 col-md-6 col-lg-4 col-xl-4">
                                    <div class="products-single fix">
                                        <div class="box-img-hover">
                                            <div class="type-lb">
                                                <p class="sale">Sale</p>
                                            </div>
                                            <img src="{{url('/image' ).'/'. $product->product->image[0]['image']}}" class="img-fluid" alt="Image">
                                            {{--<div class="mask-icon">--}}
                                                {{--<ul>--}}
                                                    {{--<li><a href="{{URL::to('/product-item').'/'.$product['id']}}" data-toggle="tooltip" data-placement="right" title="View"><i class="fas fa-eye"></i></a></li>--}}
                                                    {{--<li><a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="fas fa-sync-alt"></i></a></li>--}}
                                                    {{--@if($id != $product["user_id"])--}}
                                                        {{--<li><a href="#" data-toggle="tooltip" data-placement="right" title="Add to Wishlist"><i class="far fa-heart"></i></a></li>--}}
                                                    {{--@endif--}}
                                                {{--</ul>--}}
                                                {{--@if($id != $product["user_id"])--}}
                                                    {{--<a class="cart" href="{{URL::to('/product-item').'/'.$product['id']}}">Add to Cart</a>--}}
                                                {{--@endif--}}
                                            {{--</div>--}}
                                        </div>
                                        <div class="why-text">
                                            <h4>{{$product ->product['name']}}</h4>
                                            <h5>{{$product ->product['price']}}</h5>
                                            <div class="star">
                                                <i class="far fa-star" data-id="1"></i>
                                                <i class="far fa-star" data-id="2"></i>
                                                <i class="far fa-star" data-id="3"></i>
                                                <i class="far fa-star" data-id="4"></i>
                                                <i class="far fa-star" data-id="5"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
@section('js')
@endsection
