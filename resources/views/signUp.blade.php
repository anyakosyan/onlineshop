<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>RegistrationForm_v1 by Colorlib</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<!-- MATERIAL DESIGN ICONIC FONT -->
		<link rel="stylesheet" href="{{url('fonts/material-design-iconic-font/css/material-design-iconic-font.min.css')}}">
		{{-- <link rel="stylesheet" href="{{asset('fonts/material-design-iconic-font/css/material-design-iconic-font.min.css')}}"> --}}
		<!-- STYLE CSS -->
		<link rel="stylesheet" href="{{URL::asset('css/style.css')}}">
	</head>

	<body>

		<div class="wrapper" style="background-image: url('images/bg-registration-form-1.jpg')">
			<div class="inner">
				<div class="image-holder">
					<img src="{{url('images/registration-form-1.jpg')}}" alt="">
				</div>
				<form action="{{URL::to('/signup')}}" method="post" class="regist_form">
					<h3>Registration Form</h3>
					{{csrf_field()}}
					<div class="form-wrapper">
						<input type="text" placeholder="First Name" class="form-control" name="name" value="{{old('name')}}">
						@if ($errors->has('name'))
				    	<div class="error">{{ $errors->first('name') }}</div>
				    		{{-- <input type="hidden" name="_token" value='{{csrf_token()}}'> --}}
						@endif
	
					</div>
					<div class="form-wrapper">
						<input type="text" placeholder="Last Name" class="form-control" name="surname" value="{{old('name')}}">
						@if ($errors->has('surname'))
				    	<div class="error">{{ $errors->first('surname') }}</div>
						@endif
					</div>
					<div class="form-wrapper">
						<input type="text" placeholder="Email Address" class="form-control" name="email" value="{{old('name')}}">
						<i class="zmdi zmdi-email"></i>
						@if ($errors->has('email'))
				    	<div class="error">{{ $errors->first('email') }}</div>
						@endif
					</div>
					<div class="form-wrapper">
						<input type="number" id="" class="form-control" placeholder="age" name="age" value="{{old('name')}}">
						@if ($errors->has('age'))
				    	<div class="error">{{ $errors->first('age') }}</div>
						@endif
					</div>
					<div class="form-wrapper">
						<input type="password" placeholder="Password" class="form-control" name='password'>
						<i class="zmdi zmdi-lock"></i>
						@if ($errors->has('password'))
				    	<div class="error">{{ $errors->first('password') }}</div>
						@endif
					</div>
					<div class="form-wrapper">
						<input type="password" placeholder="Confirm Password" class="form-control" name="confpassword">
						<i class="zmdi zmdi-lock"></i>
						@if ($errors->has('confpassword'))
				    	<div class="error">{{ $errors->first('confpassword') }}</div>
						@endif
					</div>
					<button>Register
						<i class="zmdi zmdi-arrow-right"></i>
					</button>
				</form>
			</div>
		</div>
		
	</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>