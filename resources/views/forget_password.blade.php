<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="{{URL::asset('css/style-shop.css')}}">
    <title>Forgot password</title>
</head>
<body class="forget-pass_main">
<div class="container">
    <h1 class="text-center">Please write new password</h1>
    <form action="{{URL::to('/forgot-password')}}" method="post" id="edit_pass" class="m-auto">
        {{csrf_field()}}
        <div class="form-group">
            <label for="new-pass-code">Your code <small>(Please see email message code)</small></label>
            <input id="new-pass-code" type="text" placeholder="" class="form-control" name="code">
            @if($errors ->has('code'))
                <span class="error">{{$errors ->first()}}</span>
            @endif
            @error('errorCode')
            <span class="error">{{ $message }}</span>
            @enderror
        </div>
        <div class="form-group">
            <label for="new-pass">New Password</label>
            <input id="new-pass" type="text" placeholder="" class="form-control" name="password">
            @if($errors ->has('password'))
                <span class="error">{{$errors ->first()}}</span>
            @endif
        </div>
        <div class="form-group">
            <label for="new-pass-conf">Confirm New Password</label>
            <input id="new-pass-conf" type="text" placeholder="" class="form-control" name="confPassword">
            @if($errors ->has('confPassword'))
                <span class="error">{{$errors ->first()}}</span>
            @endif
        </div>
        <div class="form-group text-center">
            <button class="btn" id="send-new-pass">Submit</button>
        </div>

    </form>
</div>

</body>
</html>
