<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>RegistrationForm_v1 by Colorlib</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<!-- MATERIAL DESIGN ICONIC FONT -->
		<link rel="stylesheet" href="{{url('fonts/material-design-iconic-font/css/material-design-iconic-font.min.css')}}">
		{{-- <link rel="stylesheet" href="{{asset('fonts/material-design-iconic-font/css/material-design-iconic-font.min.css')}}"> --}}
		<!-- STYLE CSS -->
		<link rel="stylesheet" href="{{URL::asset('css/style.css')}}">
		<link rel="stylesheet" href="{{URL::asset('css/style-shop.css')}}">
	</head>

	<body>

		<div class="wrapper" style="background-image: url('images/bg-registration-form-1.jpg');">
			<div class="inner">
				<div class="image-holder">
					<img src="{{url('images/registration-form-1.jpg')}}" alt="">
				</div>
				<form action="{{URL::to('/login')}}" method="post" class="login_form">
					<h3>Login Form</h3>
					{{csrf_field()}}
					<div class="form-wrapper">
						
						<input type="text" placeholder="Email Address" class="form-control" name="email" value="{{old('name')}}">
						<i class="zmdi zmdi-email"></i>
						@if ($errors->has('email'))
				    	<div class="error">{{ $errors->first('email') }}</div>
						@endif
						@error('emailerror')
                        <p>{{ $message }}</p>
						@enderror
					</div>
					<div class="form-wrapper">
						<input type="password" placeholder="Password" class="form-control" name="password">
						<i class="zmdi zmdi-lock"></i>
						@if ($errors->has('password'))
				    	<div class="error">{{ $errors->first('password') }}</div>
						@endif
						@error('passworderror')
                        <div class="error">{{ $message }}</div>
						@enderror
						@error('activateUser')
                        <div class="error">{{ $message }}</div>
						@enderror
						@error('blockUser')
						<div class="error">{{ $message }}</div>
						@enderror
						@error('forgetPassword')
						<a href="@error('address'){{URL::to('/forgot-password').'/'.$message}}@enderror">{{ $message }}</a>
						@enderror
					</div>
					<button>Login
						<i class="zmdi zmdi-arrow-right"></i>
					</button>
				</form>
			</div>
		</div>
		
	</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>
